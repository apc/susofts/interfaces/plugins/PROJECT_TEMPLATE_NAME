#include "PROJECT_TEMPLATE_NAMEConfig.hpp"
#include "ui_PROJECT_TEMPLATE_NAMEConfig.h"

#include <QSettings>

#include "PROJECT_TEMPLATE_NAME.hpp"

bool PROJECT_TEMPLATE_NAMEConfigObject::operator==(const SConfigObject &o) const noexcept
{
	if (!SConfigObject::operator==(o))
		return false;
	const PROJECT_TEMPLATE_NAMEConfigObject &oc = static_cast<const PROJECT_TEMPLATE_NAMEConfigObject &>(o);
	return jsonColor == oc.jsonColor && indent == oc.indent;
}

void PROJECT_TEMPLATE_NAMEConfigObject::write() const
{
	QSettings settings;
	settings.beginGroup(PROJECT_TEMPLATE_NAME::_name());

	settings.setValue("jsonColor", jsonColor);
	settings.setValue("indent", indent);

	settings.endGroup();
}

void PROJECT_TEMPLATE_NAMEConfigObject::read()
{
	QSettings settings;
	settings.beginGroup(PROJECT_TEMPLATE_NAME::_name());

	jsonColor = settings.value("jsonColor", true).toBool();
	indent = settings.value("indent", true).toBool();

	settings.endGroup();
}

PROJECT_TEMPLATE_NAMEConfig::PROJECT_TEMPLATE_NAMEConfig(SPluginInterface *owner, QWidget *parent) : SConfigWidget(owner, parent), ui(std::make_unique<Ui::PROJECT_TEMPLATE_NAMEConfig>())
{
	ui->setupUi(this);
}

PROJECT_TEMPLATE_NAMEConfig::~PROJECT_TEMPLATE_NAMEConfig() = default;

SConfigObject *PROJECT_TEMPLATE_NAMEConfig::config() const
{
	auto *tmp = new PROJECT_TEMPLATE_NAMEConfigObject();
	tmp->jsonColor = ui->colorCheck->isChecked();
	tmp->indent = ui->indentCheck->isChecked();
	return tmp;
}

void PROJECT_TEMPLATE_NAMEConfig::setConfig(const SConfigObject *conf)
{
	const auto *config = dynamic_cast<const PROJECT_TEMPLATE_NAMEConfigObject *>(conf);
	if (config)
	{
		ui->colorCheck->setChecked(config->jsonColor);
		ui->indentCheck->setChecked(config->indent);
	}
}

void PROJECT_TEMPLATE_NAMEConfig::reset()
{
	PROJECT_TEMPLATE_NAMEConfigObject config;
	config.read();
	setConfig(&config);
}

void PROJECT_TEMPLATE_NAMEConfig::restoreDefaults()
{
	PROJECT_TEMPLATE_NAMEConfigObject config;
	setConfig(&config);
}
