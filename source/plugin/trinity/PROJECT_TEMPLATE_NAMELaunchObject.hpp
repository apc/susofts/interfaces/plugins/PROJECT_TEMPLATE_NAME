/*
© Copyright CERN 2000-2023. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef PROJECT_TEMPLATE_NAMELAUNCHOBJECT_HPP
#define PROJECT_TEMPLATE_NAMELAUNCHOBJECT_HPP

#include <interface/SLaunchInterface.hpp>

class PROJECT_TEMPLATE_NAMELaunchObject : public SLaunchObject, SLaunchThread
{
	Q_OBJECT

public:
	PROJECT_TEMPLATE_NAMELaunchObject(SPluginInterface *owner, QObject *parent = nullptr) : SLaunchObject(owner, parent), SLaunchThread() {}
	virtual ~PROJECT_TEMPLATE_NAMELaunchObject() override = default;

	// SGraphicalInterface
	virtual QString exepath() const override
	{
		static QString app = "./dummy/app.exe";
		return app;
	}
	virtual bool isRunning() const noexcept override { return _isRunning; }
	virtual QString error() const override { return _stopForced ? "Stop forced by user, no result computed" : ""; }

	// SLaunchThread
	virtual void run() override;

public slots:
	// SGraphicalInterface
	virtual void launch(const QString &) override;
	virtual void stop() override;

private:
	bool _isRunning = false;
	bool _stopForced = false;
};

#endif // PROJECT_TEMPLATE_NAMELAUNCHOBJECT_HPP
