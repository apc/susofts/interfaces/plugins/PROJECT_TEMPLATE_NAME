#include "PROJECT_TEMPLATE_NAMELaunchObject.hpp"

#include <chrono>
#include <thread>

#include "PROJECT_TEMPLATE_NAME.hpp"

void PROJECT_TEMPLATE_NAMELaunchObject::run()
{
	_isRunning = true;
	emit started();
	for (unsigned int i = 1; i <= 10; i++)
	{
		if (stopRequested())
		{
			_stopForced = true;
			break;
		}
		emit loading(i * 10, QString("Step %1 over %2").arg(i).arg(10));
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
	_isRunning = false;
	emit finished(!_stopForced);
}

void PROJECT_TEMPLATE_NAMELaunchObject::launch(const QString &)
{
	std::thread t([this]() -> void { run(); });
	t.detach();
}

void PROJECT_TEMPLATE_NAMELaunchObject::stop()
{
	requestStop();
}
