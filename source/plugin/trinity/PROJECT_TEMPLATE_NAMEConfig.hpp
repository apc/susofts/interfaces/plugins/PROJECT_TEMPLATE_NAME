/*
© Copyright CERN 2000-2023. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef PROJECT_TEMPLATE_NAMECONFIG_HPP
#define PROJECT_TEMPLATE_NAMECONFIG_HPP

#include <memory>

#include <interface/SConfigInterface.hpp>

namespace Ui
{
class PROJECT_TEMPLATE_NAMEConfig;
}

struct PROJECT_TEMPLATE_NAMEConfigObject : public SConfigObject
{
	virtual ~PROJECT_TEMPLATE_NAMEConfigObject() override = default;

	bool operator==(const SConfigObject &o) const noexcept override;

	virtual void write() const override;
	virtual void read() override;

	/** Tells if the JSON plugin should have syntaxic coloration */
	bool jsonColor = true;
	/** Tells if the JSON plugin should indent on load */
	bool indent = true;
};

class PROJECT_TEMPLATE_NAMEConfig : public SConfigWidget
{
	Q_OBJECT

public:
	PROJECT_TEMPLATE_NAMEConfig(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~PROJECT_TEMPLATE_NAMEConfig();

	// SConfigInterface
	virtual SConfigObject *config() const override;
	virtual void setConfig(const SConfigObject *conf) override;
	virtual void reset() override;
	virtual void restoreDefaults() override;

private:
	std::unique_ptr<Ui::PROJECT_TEMPLATE_NAMEConfig> ui;
};

#endif // PROJECT_TEMPLATE_NAMECONFIG_HPP
