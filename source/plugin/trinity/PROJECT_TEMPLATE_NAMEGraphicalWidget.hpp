/*
© Copyright CERN 2000-2023. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef PROJECT_TEMPLATE_NAMEGRAPHICALWIDGET_HPP
#define PROJECT_TEMPLATE_NAMEGRAPHICALWIDGET_HPP

#include <interface/STabInterface.hpp>

class PROJECT_TEMPLATE_NAMECSVWidget;
class PROJECT_TEMPLATE_NAMEJSONWidget;
class PROJECT_TEMPLATE_NAMETableEditorWidget;

class PROJECT_TEMPLATE_NAMEGraphicalWidget : public STabInterface
{
	Q_OBJECT

public:
	PROJECT_TEMPLATE_NAMEGraphicalWidget(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~PROJECT_TEMPLATE_NAMEGraphicalWidget() override = default;

protected:
	// SgraphicalWidget
	virtual bool _save(const QString &path) override;
	virtual bool _open(const QString &path) override;
	virtual void _newEmpty() override;

private:
	PROJECT_TEMPLATE_NAMECSVWidget *_csvTab;
	PROJECT_TEMPLATE_NAMEJSONWidget *_jsonTab;
	PROJECT_TEMPLATE_NAMETableEditorWidget *_tableTab;
};

#endif // PROJECT_TEMPLATE_NAMEGRAPHICALWIDGET_HPP
