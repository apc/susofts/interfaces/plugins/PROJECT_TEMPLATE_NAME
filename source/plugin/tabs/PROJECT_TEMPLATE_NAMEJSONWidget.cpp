#include "PROJECT_TEMPLATE_NAMEJSONWidget.hpp"

#include <fstream>
#include <sstream>

#include <Qsci/qscilexerjson.h>

#include <Logger.hpp>
#include <QtStreams.hpp>
#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <editors/text/TextEditor.hpp>
#include <editors/text/TextUtils.hpp>
#include <io/ShareablePointsListIOJson.hpp>
#include <utils/SettingsManager.hpp>

#include "PROJECT_TEMPLATE_NAME.hpp"
#include "trinity/PROJECT_TEMPLATE_NAMEConfig.hpp"

PROJECT_TEMPLATE_NAMEJSONWidget::PROJECT_TEMPLATE_NAMEJSONWidget(SPluginInterface *owner, QWidget *parent) : TextEditorWidget(owner, parent)
{
	updateUi(owner->name());

	// setup editor
	editor()->setMimeTypesSupport(
		{clipboardMimeType().toStdString(), "application/json"}, [this](const QMimeData *mimedata) -> QByteArray { return fromMime(mimedata); },
		[this](const QByteArray &text, QMimeData *mimedata) -> QMimeData * { return toMime(text, mimedata); });
}

ShareablePointsList PROJECT_TEMPLATE_NAMEJSONWidget::getContent() const
{
	return ShareablePointsListIOJSon().read(toByteArray(editor()->text(), editor()->isUtf8()).constData());
}

void PROJECT_TEMPLATE_NAMEJSONWidget::setContent(const ShareablePointsList &spl)
{
	TextEditorWidget::setContent(spl);
	ShareablePointsListIOJSon json;
	auto config = SettingsManager::settings().settings<PROJECT_TEMPLATE_NAMEConfigObject>(PROJECT_TEMPLATE_NAME::_name());
	json.isIndented(config.indent);
	editor()->setText(fromByteArray(json.write(spl).c_str(), editor()->isUtf8()));
}

void PROJECT_TEMPLATE_NAMEJSONWidget::updateUi(const QString &pluginName)
{
	TextEditorWidget::updateUi(pluginName);
	if (pluginName != PROJECT_TEMPLATE_NAME::_name())
		return;
	auto config = SettingsManager::settings().settings<PROJECT_TEMPLATE_NAMEConfigObject>(pluginName);

	if (!config.jsonColor)
		editor()->setLexer(nullptr);
	else if (!editor()->lexer())
		editor()->setLexer(new QsciLexerJSON(editor()));
}

void PROJECT_TEMPLATE_NAMEJSONWidget::_newEmpty()
{
	setContent(PROJECT_TEMPLATE_NAME::_pointsTemplate);
}

QString PROJECT_TEMPLATE_NAMEJSONWidget::clipboardMimeType() const
{
	return "application/vnd.cern.susoft.sugl." + PROJECT_TEMPLATE_NAME::_name() + ".json";
}

QByteArray PROJECT_TEMPLATE_NAMEJSONWidget::fromMime(const QMimeData *mimedata)
{
	ShareablePointsListIOJSon iojson;
	return contentFromMime(mimedata, iojson);
}

QMimeData *PROJECT_TEMPLATE_NAMEJSONWidget::toMime(const QByteArray &text, QMimeData *mimedata)
{
	ShareablePointsListIOJSon iojson;
	return contentToMime(text, mimedata, iojson);
}
