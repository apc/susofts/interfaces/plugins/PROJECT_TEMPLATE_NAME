/*
© Copyright CERN 2000-2023. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef PROJECT_TEMPLATE_NAMETABLEEDITOR_HPP
#define PROJECT_TEMPLATE_NAMETABLEEDITOR_HPP

#include <editors/table/TableEditor.hpp>

class PROJECT_TEMPLATE_NAMETableEditor : public TableEditor
{
public:
	PROJECT_TEMPLATE_NAMETableEditor(QWidget *parent = nullptr);
	virtual ~PROJECT_TEMPLATE_NAMETableEditor() override = default;

	virtual void paste() override;
	virtual void copy() override;
	virtual QString getRowAnnotation(const QItemSelection &) const override { return QString("Annotations test"); }
	virtual void precision(int precision) override;
	virtual int precision() override;

protected:
	virtual ShareablePoint parseRow(int row) const override;
	virtual void setPoint(int row, const ShareablePoint &point) override;
};

#endif // PROJECT_TEMPLATE_NAMETABLEEDITOR_HPP
