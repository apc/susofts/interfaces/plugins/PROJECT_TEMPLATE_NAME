#include "PROJECT_TEMPLATE_NAMETableEditor.hpp"

#include <QMimeData>

#include <Logger.hpp>
#include <ShareablePoints/IShareablePointsListIO.hpp>
#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>
#include <editors/table/CommandTable.hpp>
#include <editors/table/TableDoubleDelegate.hpp>
#include <editors/table/TableStringDelegate.hpp>
#include <utils/ClipboardManager.hpp>

PROJECT_TEMPLATE_NAMETableEditor::PROJECT_TEMPLATE_NAMETableEditor(QWidget *parent) : TableEditor(parent)
{
	std::vector<QString> headers{"name",
		// position
		"x", "y", "z", "sigmax", "sigmay", "sigmaz", "isfreex", "isfreey", "isfreez",
		// comments
		"inlineComment", "headerComment", "active", "extraInfos"};
	setColumnCount((int)headers.size());
	for (unsigned int i = 0; i < headers.size(); i++)
		setHorizontalHeaderItem(i, createItem(headers[i]));

	// Set double delegates
	setItemDelegateForColumn(0, new TableStringDelegate(this, "^[a-zA-Z0-9_]+$"));
	setItemDelegateForColumn(1, new TableDoubleDelegate(this));
	setItemDelegateForColumn(2, new TableDoubleDelegate(this));
	setItemDelegateForColumn(3, new TableDoubleDelegate(this));
	setItemDelegateForColumn(4, new TableDoubleDelegate(this));
	setItemDelegateForColumn(5, new TableDoubleDelegate(this));
	setItemDelegateForColumn(6, new TableDoubleDelegate(this));
}

void PROJECT_TEMPLATE_NAMETableEditor::paste()
{
	blockSignals(true);
	try
	{
		auto spl = ClipboardManager::getClipboardManager().paste();
		auto points = spl.getRootFrame().getAllPoints();
		if (!selectionModel()->selectedRows().isEmpty()) // if row selected - paste on row and below (overwrite)
		{
			for (unsigned int i = 0; i < points.size(); i++)
				setPoint(selectionModel()->selectedRows()[0].row() + i, *points[i]);
		}
		else if (!selectedItems().isEmpty()) // if only item(s) selected - insert new points below
		{
			for (unsigned int i = 0; i < points.size(); i++)
			{
				insertRow(selectedItems().last()->row() + 1 + i);
				setPoint(selectedItems().last()->row() + 1 + i, *points[i]);
			}
		}
		else // if no selection - append to the end of table
		{
			for (auto point : points)
				setPoint(rowCount(), *point);
		}
	}
	catch (const std::exception &e)
	{
		logInfo() << "Unable to paste,'" << e.what() << "'";
	}
	blockSignals(false);
	emit actionDone(new CommandTable(this));
}

void PROJECT_TEMPLATE_NAMETableEditor::copy()
{
	auto selectionToText = [this](char separator) -> QByteArray {
		QByteArray result;
		auto items = selectedItems();
		for (int i = 0; i < items.count(); i++)
		{
			result += items.at(i)->text();
			if (i + 1 < items.count() && (items.at(i)->row() != items.at(i + 1)->row()))
				result += '\n';
			else
				result += separator;
		}
		return result.left(result.length() - 1);
	};

	auto &clipboard = ClipboardManager::getClipboardManager();
	ShareablePointsList spl;
	ShareableFrame &frame = spl.getRootFrame();
	auto mimedata = new QMimeData();

	try
	{
		if (selectionModel()->selectedRows().isEmpty())
			throw SPIOException("Only part of the row(s) was chosen.");
		for (auto &rowId : selectionModel()->selectedRows())
			frame.add(new ShareablePoint(parseRow(rowId.row())));
		clipboard.copy(spl, mimedata);
	}
	catch (const std::exception &e)
	{
		logInfo() << "Impossible to serialize the selection for the copy: '" << e.what() << "'";
		const auto *exception = dynamic_cast<const SPIOException *>(&e);
		if (exception)
			logDebug() << exception->contents();
		mimedata->setData("text/csv", selectionToText(','));
	}
	mimedata->setData("text/plain", selectionToText('\t'));
	ClipboardManager::getClipboardManager().putInClipboard(mimedata);
}

void PROJECT_TEMPLATE_NAMETableEditor::precision(int precision)
{
	// for double delegates
	for (int col = 1; col <= 6; col++)
	{
		auto columnDelegate = qobject_cast<TableDoubleDelegate *>(itemDelegateForColumn(col));
		if (columnDelegate)
			columnDelegate->precision(precision);
	}
}

int PROJECT_TEMPLATE_NAMETableEditor::precision()
{
	auto columnDelegate = qobject_cast<TableDoubleDelegate *>(itemDelegateForColumn(1));
	if (columnDelegate)
		return columnDelegate->precision();
	return -1; // no TableDoubleDelegate
}

ShareablePoint PROJECT_TEMPLATE_NAMETableEditor::parseRow(int row) const
{
	ShareablePoint sp;
	sp.name = item(row, 0)->text().toStdString();
	if (sp.name.empty())
		throw SPIOException("Can't convert a row with empty name to a ShareablePoint.");
	sp.position = ShareablePosition{
		item(row, 1)->text().toDouble(),
		item(row, 2)->text().toDouble(),
		item(row, 3)->text().toDouble(),
		item(row, 4)->text().toDouble(),
		item(row, 5)->text().toDouble(),
		item(row, 6)->text().toDouble(),
		item(row, 7)->text() == "true" ? true : false,
		item(row, 8)->text() == "true" ? true : false,
		item(row, 9)->text() == "true" ? true : false,
	};
	sp.inlineComment = item(row, 10)->text().toStdString();
	sp.headerComment = item(row, 11)->text().toStdString();
	sp.active = item(row, 12)->text() == "true" ? true : false;
	auto tempSEI = ShareableExtraInfos{};
	tempSEI.addExtraInfo(std::string("TableExtraInfo"), item(row, 13)->text().toStdString());
	sp.extraInfos = ShareableExtraInfos{tempSEI};
	return sp;
}

void PROJECT_TEMPLATE_NAMETableEditor::setPoint(int row, const ShareablePoint &point)
{
	if (rowCount() <= row)
		insertRow(row);

	setItem(row, 0, createItem(QString::fromStdString(point.name)));
	setItem(row, 1, createItem(point.position.x));
	setItem(row, 2, createItem(point.position.y));
	setItem(row, 3, createItem(point.position.z));
	setItem(row, 4, createItem(point.position.sigmax));
	setItem(row, 5, createItem(point.position.sigmay));
	setItem(row, 6, createItem(point.position.sigmaz));
	setItem(row, 7, createItem(point.position.isfreex));
	setItem(row, 8, createItem(point.position.isfreey));
	setItem(row, 9, createItem(point.position.isfreez));
	setItem(row, 10, createItem(QString::fromStdString(point.inlineComment)));
	setItem(row, 11, createItem(QString::fromStdString(point.headerComment)));
	setItem(row, 12, createItem(point.active));
	std::string extInfos;
	for (const auto &[key, info] : point.extraInfos.getMap())
		extInfos += info + ';';
	setItem(row, 13, createItem(QString::fromStdString(extInfos)));
}
