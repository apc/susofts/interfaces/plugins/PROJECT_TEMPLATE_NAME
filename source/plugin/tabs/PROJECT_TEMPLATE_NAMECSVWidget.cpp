#include "PROJECT_TEMPLATE_NAMECSVWidget.hpp"

#include <fstream>
#include <sstream>

#include <Logger.hpp>
#include <QtStreams.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <editors/text/TextEditor.hpp>
#include <editors/text/TextUtils.hpp>
#include <io/ShareablePointsListIOCSV.hpp>

#include "PROJECT_TEMPLATE_NAME.hpp"

PROJECT_TEMPLATE_NAMECSVWidget::PROJECT_TEMPLATE_NAMECSVWidget(SPluginInterface *owner, QWidget *parent) : TextEditorWidget(owner, parent)
{
	updateUi(owner->name());

	// setup editor
	editor()->setMimeTypesSupport(
		{clipboardMimeType().toStdString(), "text/csv"}, [this](const QMimeData *mimedata) -> QByteArray { return fromMime(mimedata); },
		[this](const QByteArray &text, QMimeData *mimedata) -> QMimeData * { return toMime(text, mimedata); });
}

ShareablePointsList PROJECT_TEMPLATE_NAMECSVWidget::getContent() const
{
	return ShareablePointsListIOCSV().read(toByteArray(editor()->text(), editor()->isUtf8()).constData());
}

void PROJECT_TEMPLATE_NAMECSVWidget::setContent(const ShareablePointsList &spl)
{
	TextEditorWidget::setContent(spl);
	editor()->setText(fromByteArray(ShareablePointsListIOCSV().write(spl).c_str(), editor()->isUtf8()));
}

void PROJECT_TEMPLATE_NAMECSVWidget::_newEmpty()
{
	setContent(PROJECT_TEMPLATE_NAME::_pointsTemplate);
}

QString PROJECT_TEMPLATE_NAMECSVWidget::clipboardMimeType() const
{
	return "application/vnd.cern.susoft.sugl." + PROJECT_TEMPLATE_NAME::_name() + ".csv";
}

QByteArray PROJECT_TEMPLATE_NAMECSVWidget::fromMime(const QMimeData *mimedata)
{
	ShareablePointsListIOCSV iocsv;
	if (editor())
	{
		int linefrom, indexfrom, lineto, indexto;
		editor()->getSelection(&linefrom, &indexfrom, &lineto, &indexto);
		iocsv.writeHeaders(editor()->text().isEmpty() || (linefrom == 0 && indexfrom == 0 && lineto == editor()->lines() - 1 && indexto == editor()->lineLength(lineto)));
	}
	else
		iocsv.writeHeaders(false);

	QByteArray text = contentFromMime(mimedata, iocsv);
	if (text.isEmpty())
	{
		iocsv.changeMimeType("Csv"); // Excel
		text = contentFromMime(mimedata, iocsv);
	}
	return text;
}

QMimeData *PROJECT_TEMPLATE_NAMECSVWidget::toMime(const QByteArray &text, QMimeData *mimedata)
{
	ShareablePointsListIOCSV iocsv;
	return contentToMime(text, mimedata, iocsv);
}
