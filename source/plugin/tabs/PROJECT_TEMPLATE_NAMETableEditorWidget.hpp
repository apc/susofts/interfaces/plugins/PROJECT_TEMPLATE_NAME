/*
© Copyright CERN 2000-2023. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef PROJECT_TEMPLATE_NAMETABLEEDITORWIDGET_HPP
#define PROJECT_TEMPLATE_NAMETABLEEDITORWIDGET_HPP

#include <editors/table/TableEditorWidget.hpp>

class PROJECT_TEMPLATE_NAMETableEditorWidget : public TableEditorWidget
{
public:
	PROJECT_TEMPLATE_NAMETableEditorWidget(TableEditor *editor, SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~PROJECT_TEMPLATE_NAMETableEditorWidget() override;

public slots:
	// SGraphicalWidget
	virtual void setContent(const ShareablePointsList &spl) override;
	virtual void undo() override;
	virtual void redo() override;

protected:
	// SGraphicalWidget
	virtual bool _save(const QString &path) override;
	virtual bool _open(const QString &path) override;
	virtual void _newEmpty() override {}

private:
	/** pimpl */
	class _PROJECT_TEMPLATE_NAMETableEditorWidget_pimpl;
	std::unique_ptr<_PROJECT_TEMPLATE_NAMETableEditorWidget_pimpl> _pimpl;
};

#endif // PROJECT_TEMPLATE_NAMETABLEEDITORWIDGET_HPP
