/*
© Copyright CERN 2000-2023. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef PROJECT_TEMPLATE_NAME_HPP
#define PROJECT_TEMPLATE_NAME_HPP

#include <ShareablePoints/ShareablePointsList.hpp>
#include <interface/SPluginInterface.hpp>

class PROJECT_TEMPLATE_NAME : public QObject, public SPluginInterface
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID SPluginInterfaceIID)
	Q_INTERFACES(SPluginInterface)

public:
	PROJECT_TEMPLATE_NAME(QObject *parent = nullptr) : QObject(parent) {}
	virtual ~PROJECT_TEMPLATE_NAME() override = default;

	virtual const QString &name() const noexcept override { return PROJECT_TEMPLATE_NAME::_name(); }
	virtual QIcon icon() const override;

	virtual void init() override;
	virtual void terminate() override;

	virtual bool hasConfigInterface() const noexcept override { return true; }
	virtual SConfigWidget *configInterface(QWidget *parent = nullptr) override;
	virtual bool hasGraphicalInterface() const noexcept override { return true; }
	virtual SGraphicalWidget *graphicalInterface(QWidget *parent = nullptr) override;
	virtual bool hasLaunchInterface() const noexcept override { return true; }
	virtual SLaunchObject *launchInterface(QObject *parent = nullptr) override;

	virtual QString getExtensions() const noexcept override;
	virtual bool isMonoInstance() const noexcept override { return false; }

public:
	static const QString &_name();
	static const ShareablePointsList _pointsTemplate;
};

#endif // PROJECT_TEMPLATE_NAME_HPP
