#
# CMake file for PROJECT_TEMPLATE_NAME

cmake_minimum_required(VERSION 3.10)

#############################################################
#
# PROJECT NAME
#

set(PROJECT_NAME PROJECT_TEMPLATE_NAME
	LANGUAGES C CXX)

############################
# VERSION
#

set (PLUGIN_VERSION_MAJOR "0")
set (PLUGIN_VERSION_MINOR "1")
set (PLUGIN_VERSION_PATCH "00-beta")
configure_file ("Version.in" "Version.hpp")
configure_file ("Version.in" "tests/Version.hpp")


#############################################################
#
# GLOBAL CONFIGURATION
#

set(USE_QT TRUE)
set(QT_WANTED_MODULES "Qt5Core" "Qt5Widgets" "Qt5Gui")
set(CREATE_DOC ON)
set(DOXYGEN_CONF_PATH ".")
set(ENABLE_CONSOLE ON)


### SUBMODULES - if standalone build (this file is the main CMakelists, and is not used as a submodule)
if (CMAKE_SOURCE_DIR STREQUAL CMAKE_CURRENT_SOURCE_DIR)
	set(LIB_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/../lib")
	# SUSoftCMakeCommon
	include("${LIB_ROOT}/SUSoftCMakeCommon/CMakeLists.txt")
	set(CMAKE_AUTOMOC ON)
	set(CMAKE_AUTOUIC ON)
	# SurveyLib
	set(SURVEYLIB_ROOT "${LIB_ROOT}/SurveyLib")
	add_subdirectory("${SURVEYLIB_ROOT}/source/Logs" "${CMAKE_CURRENT_BINARY_DIR}/svlLogs")
	add_subdirectory("${SURVEYLIB_ROOT}/source/Plugins" "${CMAKE_CURRENT_BINARY_DIR}/svlPlugins")
	add_subdirectory("${SURVEYLIB_ROOT}/source/Tools" "${CMAKE_CURRENT_BINARY_DIR}/svlTools")
	# SUGL
	set(SUGL_ROOT "${LIB_ROOT}/SUGL")
	add_subdirectory("${SUGL_ROOT}" ${CMAKE_CURRENT_BINARY_DIR}/sugl)
endif(CMAKE_SOURCE_DIR STREQUAL CMAKE_CURRENT_SOURCE_DIR)

### PLUGIN
file(GLOB_RECURSE PLUGIN_HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/plugin/*.hpp")
file(GLOB_RECURSE PLUGIN_SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/plugin/*.cpp")
file(GLOB_RECURSE PLUGIN_UIS "${CMAKE_CURRENT_SOURCE_DIR}/plugin/*.ui")
set(PLUGIN_RSRC
	${SUGL_ROOT}/source/resources/resources.qrc
)
############################
# Main program:
# We create the shared library holding the plugin
set(PLUGIN_ALL_SRCES
	${PLUGIN_HEADERS}
	${PLUGIN_SOURCES}
	${PLUGIN_UIS}
	${PLUGIN_RSRC}
)
add_library(${PROJECT_NAME} SHARED
	${PLUGIN_ALL_SRCES}
)
target_compile_definitions(${PROJECT_NAME} PRIVATE
	QSCINTILLA_DLL
	UIPlugins_IMPORT
)
target_include_directories(${PROJECT_NAME} PRIVATE
	${CMAKE_CURRENT_BINARY_DIR}
	${CMAKE_CURRENT_SOURCE_DIR}/plugin
	# SurveyLib
	${SURVEYLIB_ROOT}/source/Logs
	${SURVEYLIB_ROOT}/source/Plugins
	# SUGL
	${SUGL_ROOT}/source/UIPlugins
)
target_link_libraries(${PROJECT_NAME}
	# Qt
	Qt5::Core
	Qt5::Gui
	Qt5::Widgets
	# QScintilla
	qscintilla2_qt5
	# SurveyLib
	Plugins
	# SUGL
	UIPlugins
)

############################
# Tests:
add_subdirectory("tests")

############################
# Provide to SurveyPad:
set_target_properties(${PROJECT_NAME} PROPERTIES 
                                RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/$<CONFIG>/plugins/"
                                ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/$<CONFIG>/plugins/"
                                LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/$<CONFIG>/plugins/"
)
if(WIN32)
	deployqt(${PROJECT_NAME} "${CMAKE_BINARY_DIR}/$<CONFIG>/")
endif()

# IDE
file(GLOB_RECURSE CLASSIFIED_SOURCES
	LIST_DIRECTORIES TRUE
	RELATIVE "${CMAKE_CURRENT_LIST_DIR}/plugin"
	"${CMAKE_CURRENT_LIST_DIR}/plugin/*"
)
foreach(child ${CLASSIFIED_SOURCES})
	if(IS_DIRECTORY "${CMAKE_CURRENT_LIST_DIR}/plugin/${child}")
		file(GLOB childfiles
			LIST_DIRECTORIES FALSE
			"${CMAKE_CURRENT_LIST_DIR}/plugin/${child}/*.cpp"
			"${CMAKE_CURRENT_LIST_DIR}/plugin/${child}/*.h"
			"${CMAKE_CURRENT_LIST_DIR}/plugin/${child}/*.hpp"
			"${CMAKE_CURRENT_LIST_DIR}/plugin/${child}/*.ui"
		)
		source_group("plugin/${child}" FILES ${childfiles})
	endif()
endforeach()
file(GLOB childfiles
	LIST_DIRECTORIES FALSE
	"${CMAKE_CURRENT_LIST_DIR}/plugin/*.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/plugin/*.h"
	"${CMAKE_CURRENT_LIST_DIR}/plugin/*.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/plugin/*.ui"
)
source_group("plugin" FILES ${childfiles})

set_target_properties(${PROJECT_NAME} testsPROJECT_TEMPLATE_NAME PROPERTIES FOLDER "Plugins/PROJECT_TEMPLATE_NAME")
